// public/core.js
var firstApp = angular.module('firstApp', ['ngDragDrop']);

firstApp.controller('mainController', function mainController($scope, $http){
    $scope.tasks = [];
    $scope.newContent = '';
    $http.get('/tasks')
        .success(function (data, status, headers, config) {
            $scope.tasks = data;
            console.log($scope.tasks)
        })
        .error(function (data) {

        });

    $scope.addTask = function () {
        var newTask = { title: 'Tasks', content: $scope.newContent };
        $http.post('/tasks', newTask)
            .success(function () {
                $scope.tasks.push(newTask);
                console.info($scope.tasks);
            })
            .error (function () {

            });
    };
});
