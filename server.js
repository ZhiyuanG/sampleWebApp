// server.js

// set up ========================
var express  = require('express');
var app      = express();                               // create our app w/ express
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

// configuration =================

app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// routes ======================================================================

// data model --------------------------------------------------------------
var tasks = [];
tasks.push({title: 'default task', content: 'default content'});
// api ---------------------------------------------------------------------
app.get('/', function(req, res) {
    res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.get('/tasks', function(req, res) {
    res.send(tasks);
});

app.post('/tasks', function(req, res) {
    console.log(req.body);
    tasks.push(req.body);
    res.send();
    console.log(tasks);
});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");